"""
Core action logic.

Defines/describes what this package is trying to do.

Specifically, what are the project task commands we want to expose,
and what do they do?
"""


# [-Imports:Python-]
import contextlib
import functools
import typing

# [-Imports:Project-]
from project_tasks.adapters import secondary

# [-Internal-]
_LAST_FAILED_PATH = ".last-failed"
NoArgFunc = typing.Callable[[], typing.Any]


@contextlib.contextmanager
def _last_failed_was(name: str) -> typing.Iterator[None]:
    secondary.write_text_to(_LAST_FAILED_PATH, text=name)
    yield
    secondary.rm_file(_LAST_FAILED_PATH)


def _deduplicated(func: NoArgFunc) -> NoArgFunc:
    functions_run: typing.List[NoArgFunc] = []

    @functools.wraps(func)
    def _wrapper() -> typing.Any:
        nonlocal functions_run
        # EARLY RETURN
        if func in functions_run:
            return None
        functions_run.append(func)
        return func()

    return _wrapper


def _run_functions(*functions: NoArgFunc) -> None:
    for this_function in functions:
        this_function()


# [-Public-]
def restart_on_change(last_failed_first: bool) -> None:
    """
    Run command on file changes.

    Watches for changes to the files in the current directory.
    Ignores updates from paths which match the regexes in .do-not-watch.

    On launch, and when changes are detected, runs the `command` on the shell.

    Will interrupt running checks to re-run checks if new changes
    are detected.

    Will not run checks if the changed files hash the same (checks
    are initially prompted by filesystem events).
    """
    if last_failed_first:
        command = "invoke run-all --last-failed-first"
    else:
        command = "invoke run-all"

    paths_to_watch = [secondary.get_cwd()]
    path_regexes_to_ignore = secondary.lines_from(".do-not-watch")

    secondary.restart_on_change(
        command,
        paths_to_watch=paths_to_watch,
        path_regexes_to_ignore=path_regexes_to_ignore,
    )


def run_all(last_failed_first: bool = False) -> None:
    """Run all fixing/checking/commit tasks."""
    last_failed = secondary.get_text(_LAST_FAILED_PATH)
    subcommands = "fix check commit"
    if last_failed_first and last_failed:
        subcommands = f"fix {last_failed} check commit"
    secondary.run(f"invoke {subcommands}")


def commit() -> None:
    """Commit interactively to add changes and a commit message."""
    result = secondary.run_quietly("git status --porcelain")
    # EARLY RETURN
    if not result.stdout:
        secondary.report_status("nothing to commit")
        return
    while result.stdout:
        secondary.run("git commit --interactive")
        result = secondary.run_quietly("git status --porcelain")
    secondary.report_status("nothing more to commit")


def deps() -> None:
    """List dependency sources."""
    secondary.run("pipdeptree -r")


@_deduplicated
def comments() -> None:
    """Run comments."""
    with _last_failed_was("comments"):
        result = secondary.run("eradicate -r .")
        if result.stdout:
            secondary.exit_("commented code found!")


@_deduplicated
def vulture() -> None:
    """Run vulture."""
    with _last_failed_was("vulture"):
        secondary.run("vulture --sort-by-size **.py")


@_deduplicated
def mypy() -> None:
    """Run mypy."""
    with _last_failed_was("mypy"):
        secondary.run(
            "mypy"
            # default to checking for everything
            " --strict"
            # allow imports w/o types
            " --ignore-missing-imports"
            # allow decorators from untyped modules
            " --allow-untyped-decorators"
            # allow subclassing from untyped modules
            " --allow-subclassing-any"
            # allow implicit optionals
            " --implicit-optional"
            # show both line and column numbers
            " --show-column-numbers"
            f" **.py"
        )


@_deduplicated
def pyflakes() -> None:
    """Run pyflakes."""
    with _last_failed_was("pyflakes"):
        secondary.run("pyflakes .")


@_deduplicated
def spellcheck() -> None:
    """Run spellcheck."""
    with _last_failed_was("spellcheck"):
        secondary.run(
            "scspell"
            " --override-dictionary linter-config/scspell3k-dictionary.txt"
            " --use-builtin-base-dict"
            " --relative-to . **.py"
        )


@_deduplicated
def bandit() -> None:
    """Run bandit."""
    with _last_failed_was("bandit"):
        secondary.run(
            f"bandit -s"
            # tests use assert liberally
            " B101"
            f" -r ."
        )


@_deduplicated
def safety() -> None:
    """Run safety."""
    with _last_failed_was("safety"):
        secondary.run("safety check")


@_deduplicated
def xenon() -> None:
    """Run xenon."""
    with _last_failed_was("xenon"):
        secondary.run("xenon --max-absolute A --max-module A --max-average A .")


@_deduplicated
def pycodestyle() -> None:
    """Run pycodestyle."""
    with _last_failed_was("pycodestyle"):
        secondary.run(
            "pycodestyle ."
            # black wants line breaks before binary operators
            " --ignore=W503"
            # match black's 88 char default
            " --max-line-length=88"
            # nice summary
            " --statistics"
        )


@_deduplicated
def pydocstyle() -> None:
    """Run pydocstyle."""
    with _last_failed_was("pydocstyle"):
        secondary.run(
            "pydocstyle"
            # ok not to doc __init__ funcs
            " --add-ignore D107,"
            " ."
        )


@_deduplicated
def pylint() -> None:
    """Run pylint."""
    with _last_failed_was("pylint"):
        secondary.run(
            "pylint"
            # Allow data types
            " --disable=too-few-public-methods"
            # Allow the hanging indents that black uses
            " --disable=C0330"
            # This incorrectly flags pipe.Pipe defs
            " --disable=no-value-for-parameter"
            # This incorrectly flags pipe.Pipe uses
            " --disable=expression-not-assigned"
            # Covered by pyflakes
            " --disable=unused-import"
            # Because I don't want to wrap every mypy generic
            #   type annotation with an overload and a TYPE_CHECKING
            #   flag.
            " --disable=unsubscriptable-object"
            # Because we use vulture, and this only ever seems to catch
            #   vulture whitelisting.
            " --disable=pointless-statement"
            # line length is handled earlier in the checks by pycodestyle
            " --disable=line-too-long"
            # I like comparisons to be low < med < high, which sometimes
            # violates the standard of having the number always on
            # the left
            " --disable=misplaced-comparison-constant"
            # let isort rules govern imports
            " --disable=ungrouped-imports"
            f" **.py"
        )


@_deduplicated
def isort() -> None:
    """Fixup import sorting."""
    secondary.run("isort --apply")


@_deduplicated
def black() -> None:
    """Format code."""
    secondary.run("black .")


def fix() -> None:
    """Run fixup actions."""
    _run_functions(isort, black)


def check_style() -> None:
    """Run style static checkers."""
    _run_functions(pycodestyle, pydocstyle, pylint)


def check() -> None:
    """Run static checkers."""
    _run_functions(
        check_unused, check_correctness, check_security, check_complexity, check_style
    )


def check_complexity() -> None:
    """Run complexity static checkers."""
    xenon()


def check_security() -> None:
    """Run security static checkers."""
    _run_functions(bandit, safety)


def check_correctness() -> None:
    """Run correctnes static checkers."""
    _run_functions(mypy, pyflakes, spellcheck)


def check_unused() -> None:
    """Run unused code static checkers."""
    _run_functions(comments, vulture)
