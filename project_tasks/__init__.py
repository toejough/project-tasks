"""Project tasks."""


# [-Imports:Submodule-]
from .adapters.primary import bandit
from .adapters.primary import black
from .adapters.primary import check
from .adapters.primary import check_complexity
from .adapters.primary import check_correctness
from .adapters.primary import check_security
from .adapters.primary import check_style
from .adapters.primary import check_unused
from .adapters.primary import comments
from .adapters.primary import commit
from .adapters.primary import deps
from .adapters.primary import fix
from .adapters.primary import isort
from .adapters.primary import mypy
from .adapters.primary import pycodestyle
from .adapters.primary import pydocstyle
from .adapters.primary import pyflakes
from .adapters.primary import pylint
from .adapters.primary import run_all
from .adapters.primary import safety
from .adapters.primary import spellcheck
from .adapters.primary import vulture
from .adapters.primary import watch
from .adapters.primary import xenon

# [-Vulture-]
# pylint: disable=pointless-statement
# API's
bandit
black
check
check_complexity
check_correctness
check_security
check_style
check_unused
comments
commit
deps
fix
isort
mypy
pycodestyle
pydocstyle
pyflakes
pylint
run_all
safety
spellcheck
vulture
watch
xenon
