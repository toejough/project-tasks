"""
Secondary Adapters: core -> secondary -> core.

Adapts from core actions/data expectations to the expectations of the
external tooling and back again.
"""


# [-Imports:Python-]
import atexit
import hashlib
import pathlib
import subprocess  # nosec
import sys
import threading
import time
import typing

# [-Imports:Third Party-]
import blessed
import invoke
import psutil
from watchdog import events
from watchdog import observers

# [-Linter Notes-]
# applying #nosec to subprocess because we are explicitly using
#   subprocess as a shell interface.  The users of this tool have
#   unrestricted privileges, as far as we are concerned.

# [-Internal-]
_TERMINAL = blessed.Terminal()


class _RunningJob:
    """A restartable subprocess job."""

    def __init__(self, command: str, *, process: subprocess.Popen) -> None:
        """Create a job that runs the given command when `start` is called."""
        self.command = command
        self.process = process
        self.terminated = False


class _MyHandler(events.RegexMatchingEventHandler):
    """Custom event handler for running checks on file changes."""

    def __init__(
        self,
        *args: typing.Any,
        on_change: typing.Callable[..., typing.Any],
        **kwargs: typing.Any,
    ) -> None:
        """Create a watchdog change handler."""
        super().__init__(*args, **kwargs)
        self._known_hashes: typing.Dict[pathlib.Path, str] = {}
        self._on_change = on_change

    def on_any_event(self, event: events.FileSystemEvent) -> None:
        """Task to execute whenever watchdog observes any file system event."""
        print(f"{_TERMINAL.blue('event:')} {event.event_type}")
        path_string = event.src_path
        print(f"{_TERMINAL.blue('path:')} {path_string}")
        path = pathlib.Path(path_string)
        run_checks = False
        if path.exists():
            path_bytes = path.read_bytes()
            path_hash = hashlib.blake2b(path_bytes, digest_size=24)
            path_digest = path_hash.hexdigest()
            print(f"{_TERMINAL.blue('current hash:')}  {path_digest}")
            previous_digest = self._known_hashes.get(path, None)
            _report_hash_status(previous=previous_digest, current=path_digest)
            run_checks = _should_run_checks(
                previous=previous_digest, current=path_digest
            )
            if previous_digest != path_digest:
                self._known_hashes[path] = path_digest
        else:
            print(_TERMINAL.red("Deleted path!"))
            print(_TERMINAL.yellow("Clearing hash..."))
            if path in self._known_hashes:
                del self._known_hashes[path]
            run_checks = True
        if run_checks:
            self._on_change()

    # [-Vulture-]
    on_any_event  # used by watchdog internally


def _terminate_descendants(process: psutil.Process) -> None:
    try:
        children = process.children()
    except psutil.NoSuchProcess:
        children = []
    for this_child in children:
        _terminate_descendants(this_child)
        try:
            this_child.terminate()
            this_child.wait()
        except psutil.NoSuchProcess:
            pass


def _report_hash_status(*, previous: typing.Optional[str], current: str) -> None:
    if not previous:
        print(_TERMINAL.green("New file!"))
        print(_TERMINAL.yellow("Storing hash..."))
    elif previous != current:
        print(f"{_TERMINAL.yellow('previous hash:')}" f" {previous}")
        print(_TERMINAL.yellow("different hash!"))
        print(_TERMINAL.yellow("Updating hash..."))
    else:
        print(f"previous hash: {previous}")
        print("Same hash - no action necessary")


def _should_run_checks(*, previous: typing.Optional[str], current: str) -> bool:
    if not previous:
        run_checks = True
    elif previous != current:
        run_checks = True
    else:
        run_checks = False
    return run_checks


def _report_if_waiting(job: _RunningJob) -> None:
    job.process.wait()
    if not job.terminated:
        print(_TERMINAL.green("waiting for changes..."))
        print("(hit ctrl-c to cancel)")


def _start_job(command: str) -> _RunningJob:
    """
    Start the job.

    The job will be registered to terminate on completion
    of the main process (like a daemon thread).
    """
    process = subprocess.Popen(
        command,
        executable="/usr/local/bin/fish",
        shell=True,  # nosec
        stdin=sys.stdin,
        stdout=sys.stdout,
        stderr=sys.stderr,
    )
    running_job = _RunningJob(command=command, process=process)
    threading.Thread(
        target=_report_if_waiting, args=(running_job,), daemon=True
    ).start()
    atexit.register(_terminate_job, running_job)
    return running_job


def _terminate_job(job: _RunningJob) -> str:
    """
    Terminate the job.

    Also unregisters any remaining atexit handler so that
    we're not pointlessly trying to terminate a zillion already
    completed jobs.
    """
    # process is already done
    if job.process.poll() is not None:
        atexit.unregister(_terminate_job)
        return job.command
    # terminate
    job.terminated = True
    print(_TERMINAL.white_on_red("terminating running job..."))
    try:
        process = psutil.Process(job.process.pid)
    # EARLY RETURN
    except psutil.NoSuchProcess:
        return job.command
    _terminate_descendants(process)
    try:
        process.terminate()
        process.wait()
    except psutil.NoSuchProcess:
        pass
    return job.command


def _restart_job(job: _RunningJob) -> _RunningJob:
    """Restart the job."""
    return _start_job(_terminate_job(job))


# [-Public-]
def get_cwd() -> pathlib.Path:
    """Return the current working directory."""
    return pathlib.Path.cwd()


def lines_from(path_string: str) -> typing.Sequence[str]:
    """Return the lines (if any) from the file at the given path string."""
    path = pathlib.Path(path_string)
    if not path.is_file():
        return []
    return path.read_text().splitlines()


def restart_on_change(
    command: str,
    *,
    paths_to_watch: typing.Collection[pathlib.Path],
    path_regexes_to_ignore: typing.Collection[str],
) -> None:
    """Run the command and restart it on changes to the relevant paths."""
    # run the job
    running_job = _start_job(command)
    # watch & run callback (restart job)

    def _closured_restart_job() -> None:
        nonlocal running_job
        running_job = _restart_job(running_job)

    # watchdog observer
    observer = observers.Observer()
    # what to ignore
    # create the handler
    handler = _MyHandler(
        ignore_directories=True,
        ignore_regexes=path_regexes_to_ignore,
        on_change=_closured_restart_job,
    )
    # schedule the handlers
    for this_path in paths_to_watch:
        observer.schedule(handler, str(this_path), recursive=True)
    # run the observer
    observer.start()
    # wait for it
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
        print(_TERMINAL.yellow("\nCaught ctrl-c!"))
        print(_TERMINAL.yellow("Stopping..."))
        _terminate_job(running_job)
        print()
        observer.join()


def get_text(path_str: str) -> typing.Optional[str]:
    """Get text, if any from path at string, if found."""
    path = pathlib.Path(path_str)
    return path.read_text() if path.is_file() else None


def write_text_to(path_str: str, *, text: str) -> None:
    """Write text to a path."""
    pathlib.Path(path_str).write_text(text)


def rm_file(path_str: str) -> None:
    """Remove the file at the given path."""
    pathlib.Path(path_str).unlink()


def run(command: str) -> invoke.Result:
    """Run the command."""
    result = invoke.Context().run(
        command, pty=True, echo=True, shell="/usr/local/bin/fish"  # nosec
    )
    print()
    return result


def run_quietly(command: str) -> invoke.Result:
    """Run the command."""
    result = invoke.Context().run(
        command, hide=True, shell="/usr/local/bin/fish"  # nosec
    )
    return result


def exit_(message: str) -> typing.NoReturn:
    """Exit the program via invoke's exit path."""
    raise invoke.Exit(message)


def report_status(message: str) -> None:
    """
    Report some status.

    This is low-key information - no highlighting.
    """
    print(message)
