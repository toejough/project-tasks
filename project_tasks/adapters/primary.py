"""
Primary Adapters: primary -> core -> primary.

Adapts from external tooling expectations to the expectations of the core
actions/data and back again.

Specifically, map the front-end invocation features of invoke to our actions.
"""


# [-Imports:Third Party-]
import invoke

# [-Imports:Project-]
from project_tasks.core import actions


# [-Public-]
@invoke.task
def watch(_context: invoke.Context, last_failed_first: bool = False) -> None:
    """
    Watch for changes.

    Watches for changes to the files in the current directory.
    Ignores updates from paths which match the regexes in .do-not-watch.

    On launch, and when changes are detected, runs the
    'on_change' task.

    Will interrupt running checks to re-run checks if new changes
    are detected.

    Will not run checks if the changed files hash the same (checks
    are initially prompted by filesystem events).
    """
    # build the command
    actions.restart_on_change(last_failed_first)


@invoke.task
def run_all(_context: invoke.Context, last_failed_first: bool = False) -> None:
    """Run all fixing/checking/commit tasks."""
    actions.run_all(last_failed_first)


@invoke.task
def commit(_context: invoke.Context) -> None:
    """Commit interactively to add changes and a commit message."""
    actions.commit()


@invoke.task
def deps(_context: invoke.Context) -> None:
    """List dependency sources."""
    actions.deps()


@invoke.task
def comments(_context: invoke.Context) -> None:
    """Run comments."""
    actions.comments()


@invoke.task
def vulture(_context: invoke.Context) -> None:
    """Run vulture."""
    actions.vulture()


@invoke.task
def check_unused(_context: invoke.Context) -> None:
    """Run unused code static checkers."""
    actions.check_unused()


@invoke.task
def mypy(_context: invoke.Context) -> None:
    """Run mypy."""
    actions.mypy()


@invoke.task
def pyflakes(_context: invoke.Context) -> None:
    """Run pyflakes."""
    actions.pyflakes()


@invoke.task
def spellcheck(_context: invoke.Context) -> None:
    """Run spellcheck."""
    actions.spellcheck()


@invoke.task
def check_correctness(_context: invoke.Context) -> None:
    """Run correctnes static checkers."""
    actions.check_correctness()


@invoke.task
def bandit(_context: invoke.Context) -> None:
    """Run bandit."""
    actions.bandit()


@invoke.task
def safety(_context: invoke.Context) -> None:
    """Run safety."""
    actions.safety()


@invoke.task
def check_security(_context: invoke.Context) -> None:
    """Run security static checkers."""
    actions.check_security


@invoke.task
def xenon(_context: invoke.Context) -> None:
    """Run xenon."""
    actions.xenon()


@invoke.task
def check_complexity(_context: invoke.Context) -> None:
    """Run complexity static checkers."""
    actions.check_complexity()


@invoke.task
def pycodestyle(_context: invoke.Context) -> None:
    """Run pycodestyle."""
    actions.pycodestyle()


@invoke.task
def pydocstyle(_context: invoke.Context) -> None:
    """Run pydocstyle."""
    actions.pydocstyle()


@invoke.task
def pylint(_context: invoke.Context) -> None:
    """Run pylint."""
    actions.pylint()


@invoke.task
def check_style(_context: invoke.Context) -> None:
    """Run style static checkers."""
    actions.check_style()


@invoke.task
def check(_context: invoke.Context) -> None:
    """Run static checkers."""
    actions.check()


@invoke.task
def isort(_context: invoke.Context) -> None:
    """Fixup import sorting."""
    actions.isort()


@invoke.task
def black(_context: invoke.Context) -> None:
    """Format code."""
    actions.black()


@invoke.task
def fix(_context: invoke.Context) -> None:
    """Run fixup actions."""
    actions.fix()
