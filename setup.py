"""Setup for the project."""


# [-Imports:Python-]
import pathlib
import typing

# [-Imports:Third Party-]
import setuptools


# [-Internal-]
def _get_requirements_lines_from(file_name: str) -> typing.Collection[str]:
    lines = pathlib.Path(file_name).read_text().splitlines()
    lines = list(l.strip() for l in lines)
    lines = list(l for l in lines if l and not l.startswith(("#", "-")))
    return lines


def _get_requirements_from(file_name: str) -> typing.List[str]:
    # EARLY RETURN
    if not pathlib.Path(file_name).exists():
        return []

    lines = _get_requirements_lines_from(file_name)
    requirements = [l.split("#")[0] for l in lines]

    return requirements


# [-Script-]
setuptools.setup(
    name="project-tasks",
    version="0.1.0",
    author="toejough",
    author_email="toejough@gmail.com",
    description="python project tasks",
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=(
        *_get_requirements_from("requirements/requirements.in"),
        *_get_requirements_from("requirements/binaries.in"),
    ),
    extras_require={"dev": _get_requirements_from("requirements/dev-requirements.in")},
)
