"""Build tasks."""


# [-Imports:Third Party-]
import invoke

# [-Imports:Project-]
import project_tasks

# invoke requires this name (or `namespace`, which also isn't a valid global)
ns = invoke.Collection.from_module(project_tasks)  # pylint: disable=invalid-name

# [-Vulture-]
ns  # used by invoke
